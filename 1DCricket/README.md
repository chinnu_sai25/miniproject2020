1D Cricket

By: Kalakonda Sai Shashank
    Nishanth Sachdeva

About Game:
The game resembles 1D cricket

INstructions:
1)Run "python3 game.py" command to start the game.
2)Enter "b" to bowl the ball.
3)Enter "s" to strike the ball with bat.
4)Runs to each strike corresponds with timing the ball.
5)The data gets stored in the statistics file under observations directory.

Requirements
1)Python3

About Codes:
1)board.py consists of basic matrix code over which elements are placed.
2)batsman.py consists Batsman class which creates batsman and has its functionalites.
2)bowler.py consists Bowler class which creates bowler and has its functionalites.
3)pitch.py code consists Pitch class which creates the pitch on the board.
4)game.py contains the while loop where the game run continuosly.
5)pitchStepReading.py consists PitchStepReading class which is the reason for displaying short,full and good on pitch.
6)WelcomeBoard.py displays the instructions screen before game starts.
7)ViewScores.py displays the score on screen whenever batsman strikes the ball.