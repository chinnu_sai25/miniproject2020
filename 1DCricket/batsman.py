from viewScores import ViewScores

displayscore=ViewScores()

class Batsman:
    
    def __init__(self,row,column):
        self.row=row
        self.column=column
        self.score=0
        self.isout=0
        self.miss=0
        self.striked=0
        self.scoreforpresentball=0

    def create(self,grid):
        grid[self.row][self.column]="*"
        
    def hit(self,grid,bowler):
        self.striked=1
        if(self.isout==0):
            if(bowler.ballCurrentLoc==35 or bowler.ballCurrentLoc==36):
                self.score+=4
                self.scoreforpresentball=4
                bowler.didbatsmanhit=4
            elif(bowler.ballCurrentLoc==34 or bowler.ballCurrentLoc==37):
                self.score+=2
                self.scoreforpresentball=2
                bowler.didbatsmanhit=2
            elif(bowler.ballCurrentLoc==33 or bowler.ballCurrentLoc==38):
                self.score+=1
                self.scoreforpresentball=1
                bowler.didbatsmanhit=1
            else:
                self.miss+=1
        if(self.miss==3):
            self.isout=1
            displayscore.DisplayOut(grid,3,35)

