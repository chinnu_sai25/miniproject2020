from viewScores import ViewScores

displayscore=ViewScores()

class Bowler:    
    def __init__(self,row,column):
        self.row=row
        self.column=column
        self.ballInitiated=False
        self.ballPrevLoc=0
        self.ballCurrentLoc=0
        self.stepsCountOfBall=0
        self.bowlingOption=0
        self.bowlingSpeed=0.005
        self.didbatsmanhit=0

    def create(self,grid):
        grid[self.row][self.column]="*"

    def ballInitiation(self,grid,prevLoc,currentLoc,BowlingOption):
        displayscore.ClearOut(  grid,3,35)
        if(self.ballInitiated==False):
            self.ballInitiated=True
            self.ballPrevLoc=prevLoc
            self.ballCurrentLoc=currentLoc
            self.stepsCountOfBall=0
            self.bowlingOption=BowlingOption
            self.bowlingSpeed=0.00000000000001

    def ballMovement(self,grid):
        grid[14][35]="*"
        if(self.didbatsmanhit==0):
            grid[14][self.ballPrevLoc]=" "
            grid[14][self.ballCurrentLoc]="o"
            self.ballPrevLoc=self.ballCurrentLoc
            self.ballCurrentLoc=self.ballCurrentLoc-1
            self.stepsCountOfBall=self.stepsCountOfBall+1
            if(self.stepsCountOfBall>=60):
                grid[14][self.ballPrevLoc]=" "
                self.ballInitiated=False        
        else:
            grid[14][self.ballPrevLoc]=" "
            grid[14][self.ballCurrentLoc]="o"
            self.ballPrevLoc=self.ballCurrentLoc
            self.ballCurrentLoc=self.ballCurrentLoc+1
            self.stepsCountOfBall=self.stepsCountOfBall+1
            if(self.stepsCountOfBall>=75 and self.didbatsmanhit==1):
                displayscore.ClearScore(grid,3,60)
                grid[14][self.ballPrevLoc]=" "
                self.ballInitiated=False
            elif(self.stepsCountOfBall>=85 and self.didbatsmanhit==2):
                displayscore.ClearScore(grid,3,60)
                grid[14][self.ballPrevLoc]=" "
                self.ballInitiated=False        
            elif(self.stepsCountOfBall>=100 and self.didbatsmanhit==4):
                displayscore.ClearScore(grid,3,60)
                grid[14][self.ballPrevLoc]=" "
                self.ballInitiated=False

            if(self.ballInitiated==True):
                if(self.didbatsmanhit==4):
                    displayscore.DisplayScoreFour(grid,3,60)
                elif(self.didbatsmanhit==2):
                    displayscore.DisplayScoreTwo(grid,3,60)
                elif(self.didbatsmanhit==1):
                    displayscore.DisplayScoreOne(grid,3,60)
       