import signal,os,time,random
import os
import time
import random
from subprocess import call 

def clear(): 
    # check and make call for specific operating system 
    _ = call('clear' if os.name =='posix' else 'cls') 
  
import constants
from welcome_board import welcome_screen

try:
    os.mkdir("observations")  
except:
    pass

task=open("./observations/statistics.txt","a")
from alarmexception import AlarmException
from getch import _getChUnix as getChar
from board import Board
from batsman import Batsman
from bowler import Bowler
from pitch import Pitch
from teamInfo import Team
from colorama import init, Fore
from pitchStepReading import pitchStepReading 
import shutil

#Setting up basic parameters/info for game
columns = shutil.get_terminal_size().columns
clear()

print("\n\n")
print("Enter Team Names".center(columns))
print("\n\n")
print("Enter TeamA name".center(columns))
TeamA = input()
print("Enter TeamB name".center(columns))
TeamB = input()
while(TeamA==TeamB):
    print("Enter TeamB name different from TeamA name:".center(columns))
    TeamB = input()

clear()

print("\n\n\n")
print(("WELCOME TO THE MATCH BETWEEN Team "+TeamA+" and Team "+TeamB+" ").center(columns))
print("\n\n\n")
print("Select the number of overs between 1,3 and 5".center(columns))
MatchOvers = input()
while(not(MatchOvers=="1" or MatchOvers=="3" or MatchOvers=="5")):
    print("Recheck the value entered".center(columns))
    print("\n")
    print("Select the number of overs between 1,3 and 5".center(columns))
    MatchOvers = input()
print("\n")
print(("Lets have Toss first").center(columns))
print("\n")
print(("What Team "+TeamA+" wants:(Head or tails)").center(columns))
print(("Press 0 for Heads and 1 for tails").center(columns))
TeamAtoss = input()
while(not(TeamAtoss=="1" or TeamAtoss=="0")):
    print(("Check the value entered (Press 0 for Heads and 1 for tails)").center(columns))
    TeamAtoss = input()

clear()

print("\n\n\n")
if(random.randint(0,1)==TeamAtoss):
    print(("Congrats Team "+TeamA+" You have won the Toss").center(columns))
    print("\n")
    print("Enter 0 for batting and 1 for bowling".center(columns))
    print("\n")
    TeamAplays = input()
else:
    print(("Congrats Team "+TeamB+" You have won the Toss").center(columns))
    print("\n")
    print("Enter 0 for batting and 1 for bowling".center(columns))
    print("\n")
    TeamBplays = input()
    if(TeamBplays=="0"):
        TeamAplays="1"
    else:
        TeamAplays="0"

clear()

print("\n\n\n")
if(TeamAplays=="0"):
    print(("Team "+TeamA+" plays Batting first").center(columns))
else:
    print(("Team "+TeamB+" plays Batting first").center(columns))
print("\n\n")    
print("Press Enter to start the match".center(columns))
Tostart = input()
while(not(Tostart=="")):
    print("Press Enter to start the match".center(columns))
    Tostart = input()

clear()

#Creation of some necessary variables
firstbatting=Team()
secondbatting=Team()
if(TeamAplays=="0"):
    firstbatting.name=TeamA
    secondbatting.name=TeamB
else:
    firstbatting.name=TeamB
    secondbatting.name=TeamA

firstbatting.score=0
firstbatting.wickets=0
secondbatting.score=0
secondbatting.wickets=0

firstbatting.noOfBalls=int(MatchOvers)*6
secondbatting.noOfBalls=int(MatchOvers)*6
#Creation of objects and accessing methods

#Setting up board(i.e grid)
board = Board(30,500)
board.createboard()

#Setting up pitch(i.e Upper and Lower Boundaries)
pitch = Pitch(13,100)
pitch.createpitch(board.matrix)

#Setting up Batsman
batsman=Batsman(14,35)
batsman.create(board.matrix)

#Setting up Bowler
bowler=Bowler(14,90)
bowler.create(board.matrix)

#Setting up Pitch Readings
pitchReadings = pitchStepReading(board.matrix,20,80)
pitchReadings.createReadings()

#Initializing times as 0
ballMotionTime=0

#Function for ball release when "b" is clicked
def functionalities(): 
    def alarmhandler(signum,frame):
        raise AlarmException
    def user_input(timeout=0.1):
        signal.signal(signal.SIGALRM,alarmhandler)
        signal.setitimer(signal.ITIMER_REAL,timeout)

        try:
            text=getChar()()
            signal.alarm(0)
            return text
        except AlarmException:
            pass
        signal.signal(signal.SIGALRM,signal.SIG_IGN)
        return ''

    char = user_input()
    
    if(char=='b'):
        if(bowler.ballInitiated==0):
            print("Select the bowling option: 0 for 'None', 1 for 'Full', 2 for 'Good', 3 for 'Short'")

            BowlingOptionSelected = input()

            while(not(BowlingOptionSelected=="0" or BowlingOptionSelected=="1" or BowlingOptionSelected=="2" or BowlingOptionSelected=="3")):
                print("Select the bowling option: 0 for 'None', 1 for 'Full', 2 for 'Good', 3 for 'Short'")
                BowlingOptionSelected = input()
  
            bowler.didbatsmanhit=0
            ballMotionTime=time.time()
            BowlingOptionsArray = ["Full","Good","Short"]
            task.write("\n The ball was released with the bowling option "+BowlingOptionsArray[int(BowlingOptionSelected)-1])
            if(firstbatting.noOfBalls!=0):
                firstbatting.noOfBalls=firstbatting.noOfBalls-1
            else:
                secondbatting.noOfBalls=secondbatting.noOfBalls-1
            batsman.striked=0
            bowler.ballInitiation(board.matrix,88,87,BowlingOptionSelected)

    elif(char=='s'):
        if(bowler.ballInitiated and batsman.striked==0):
            batsman.hit(board.matrix,bowler)
            if(firstbatting.noOfBalls!=0):
                task.write("\n Batsman scored "+str(batsman.scoreforpresentball))
                firstbatting.score+=batsman.scoreforpresentball
                if(batsman.isout==1):
                    task.write("\n Batsman is out")
                    firstbatting.wickets+=batsman.isout
                    batsman.isout=0
                    batsman.miss=0
                    batsman.score=0
                if(firstbatting.wickets==5):
                    task.write("\n First innings completed")
                    firstbatting.noOfBalls=0
            else:
                secondbatting.score+=batsman.scoreforpresentball
                task.write("\n Batsman scored "+str(batsman.scoreforpresentball))
                if(batsman.isout==1):
                    task.write("\n Batsman is out")
                    secondbatting.wickets+=batsman.isout
                    batsman.isout=0
                    batsman.miss=0
                    batsman.score=0
                if(secondbatting.wickets==5):
                    secondbatting.noOfBalls=0
            batsman.scoreforpresentball=0

    elif(char=='q'):
        quit()



# game starts here ,
task.write("\n\n###############################################################\n") 
task.write("\n\n Game is between "+firstbatting.name+" and "+secondbatting.name)
screen = welcome_screen(constants.TIME_BEFORE_GAME_STARTS)
screen.display()

# this will display the game welcome screen for 5 seconds


while True:
    
    os.system('tput reset') # clears the screen

    print("Team "+firstbatting.name)
    print(str(firstbatting.score)+"/"+str(firstbatting.wickets)+"Balls Left:"+str(firstbatting.noOfBalls))
    print("Team "+secondbatting.name)
    print(str(secondbatting.score)+"/"+str(secondbatting.wickets)+"Balls Left:"+str(secondbatting.noOfBalls))


    if(time.time()-ballMotionTime >= bowler.bowlingSpeed and bowler.ballInitiated==True):  
        bowler.ballMovement(board.matrix)
            
        if(bowler.bowlingOption=='1' and bowler.ballCurrentLoc==53):
            bowler.bowlingSpeed=0.001
        elif(bowler.bowlingOption=='2' and bowler.ballCurrentLoc==63):
            bowler.bowlingSpeed=0.001
        elif(bowler.bowlingOption=='3' and bowler.ballCurrentLoc==78):
            bowler.bowlingSpeed=0.001

        ballMotionTime=time.time()

    board.print()
    functionalities()
