
class pitchStepReading:
    
    def __init__(self,grid,row,column):
        self.grid=grid
        self.row=row
        self.column=column

    def createReadings(self):
        self.grid[self.row][self.column-(35)]="Full"
        self.grid[self.row][self.column-(25)]="Good"
        self.grid[self.row][self.column-(10)]="Short"

