class ViewScores:

    def __init__(self):
        self.FourText=[]
        self.TwoText=[]
        self.OneText=[]

    def DisplayScoreFour(self,grid,c,d):
        with open("./four.txt") as obj:
            for line in obj:
                self.FourText.append(line.strip('\n'))
        e=d
        for i in range(6):
            for j in range(6):
                grid[c][d]=self.FourText[i][j]
                d+=1
            d=e
            c+=1

    def DisplayScoreTwo(self,grid,c,d):
        with open("./two.txt") as obj:
            for line in obj:
                self.TwoText.append(line.strip('\n'))
        e=d
        for i in range(6):
            for j in range(6):
                grid[c][d]=self.TwoText[i][j]
                d+=1
            d=e
            c+=1

    def DisplayScoreOne(self,grid,c,d):
        with open("./one.txt") as obj:
            for line in obj:
                self.OneText.append(line.strip('\n'))
        e=d
        for i in range(6):
            for j in range(6):
                grid[c][d]=self.OneText[i][j]
                d+=1
            d=e
            c+=1

    def DisplayOut(self,grid,c,d):
        with open("./out.txt") as obj:
            for line in obj:
                self.OneText.append(line.strip('\n'))
        e=d
        for i in range(9):
            for j in range(60):
                grid[c][d]=self.OneText[i][j]
                d+=1
            d=e
            c+=1

    def ClearScore(self,grid,c,d):
        for i in range(c,c+6):
            for j in range(d,d+6):
                grid[i][j]=' '
    
    def ClearOut(self,grid,c,d):
        for i in range(c,c+9):
            for j in range(d,d+60):
                grid[i][j]=' '
