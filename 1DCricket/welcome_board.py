import constants
import time
import os

class welcome_screen:

	def __init__(self, time_before_game_starts):
		# so here we are , making a welcome screen board
		self.timer = time_before_game_starts


		# so now we have to make a board that can display the welcome board for 5 secss
		# all the instructions also need to be present here

	def display(self):
		# so now that we are here, we init the matrix, and then display the thing

		time_before_game_starts  = self.timer
		
		matrix = []

		main_board_char = ' '
		roof_and_floor_char = '#'

		for _ in range(0, 10):
			matrix.append([roof_and_floor_char] * constants.WIDTH)

		for _ in range(10 , 25):
			matrix.append([main_board_char] * constants.WIDTH)

		for _ in range( 25 , 30):
			matrix.append( [roof_and_floor_char] * constants.WIDTH)


		for t in range(time_before_game_starts):
			# display the board with the appropriate instruction
			# first up, print the board normally for 5 seconds, then go towards printing more detail

			display_time = list("GAME STARTS IN ..... " + str(self.timer - t))
			# so now we have this to display
			
			# here in this regiion, we will put the values that we need to put
			height = 15
			word = list("WELCOME")
			for i in range(len(word)):	
				matrix[height][20 + i] = word[i]

			for i in range(len(display_time)):
				matrix[height + 5][20 + i] = display_time[i]


			# now we add the data for instructions
			main_line = list("FOLLOWING ARE THE INSTRUCTIONS")
			press_word = list("PRESS")

			batting = list(" S for batting ")
			bowling = list(" B for bowling ")
			quitting = list("Q for quitting")

			for i in range(len(main_line)):	
				matrix[height][50 + i] = main_line[i]

			for i in range(len(press_word)):	
				matrix[height + 2][50 + i] = press_word[i]


			for i in range(len(batting)):	
				matrix[height + 4][50 + i] = batting[i]


			for i in range(len(bowling)):	
				matrix[height + 6][50 + i] = bowling[i]


			for i in range(len(quitting)):	
				matrix[height + 8][50 + i] = quitting[i]

			# further from here on, we shall take the matrix and simply print it out, 
			print('\033[0;0H', end='')

			for i in matrix:
				for j in i[0:(0 + constants.WIDTH)]:
					print(j, end='')
				print('')

			time.sleep(1)
			# this line is the bedrock of our welcome screen, this will make sure we actually show the instructions